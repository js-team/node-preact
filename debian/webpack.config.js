'use strict';

var fs = require('fs');
var path = require('path');
var webpack = require('webpack');

var config = {

  resolve: {
    modules: ['/usr/lib/nodejs'],
  },

  resolveLoader: {
    modules: ['/usr/lib/nodejs'],
  }
}

module.exports = config;
